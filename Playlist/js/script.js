let songs = [{
  name: 'Jingle Bells',
  isLiked: false,
}, {
  name: 'We Wish You a Merry Christmas',
  isLiked: true,
}];

const form = document.querySelector('#form');
const input = document.querySelector('#input');
const submit = document.querySelector('#submit');
const list = document.querySelector('#tasks');
const count = document.querySelector('.count');

window.addEventListener('load', function() {
  fillHtml();
  updateLocal();
})

// set to local storage array songs in key
function updateLocal () {
  localStorage.setItem('songs', JSON.stringify(songs));
}

//add new song and push to array songs
submit.addEventListener('click', () => {
  //checking for empty input
  if (!input.value) {
    return
  }
  songs.push(new Task(input.value));
  console.log(songs);
  updateLocal();
  fillHtml();
  input.value = ''
})
//if storage empty -- result empty array else array = data of localStorage
!localStorage.songs ? songs = [] : songs = JSON.parse(localStorage.getItem('songs'));
//create constructor function 
function Task(name) {
  this.name = name;
  this.isLiked = false;
}

function fillHtml () {
  list.innerHTML = '';
  if (songs.length > 0 ) {
    songs.forEach((item, index) => {
      list.innerHTML += createTemplate(item, index);
      if (songs[index].isLiked) {
        like(index);
      }
    })
  }
  countFn();
}

function createTemplate (song, index) {
  return `
  <div class="task">
    <div class="content">
      <input type="text" value="${song.name}">
    </div>
    <div class="actions" >
      <button onclick="setLike(${index})" class="edit">Like</button>
      <button onclick="deleteTask(${index})" class="delete">Delete</button>
    </div>
  </div>
  `
}

function countFn () {
  count.textContent = `${songs.length}`;
}

const deleteTask = index => {
  console.log(index);
  songs.splice(index,1);
  updateLocal();
  fillHtml();
}

function setLike (index) {
    songs[index].isLiked = !songs[index].isLiked;
    if (songs[index].isLiked) {
      like(index);
    } else {
      unlike(index);
    }
    updateLocal();
    countFn();
  }

function like(index) {
  let arrBtn = [...document.querySelectorAll('.edit')];
  let arrAction = [...document.querySelectorAll('.actions')];
  songs[index].isLiked = true;
  arrBtn[index].textContent = 'Unlike';
  arrBtn[index].classList.add('img');
  const imgLike = document.createElement('img');
  imgLike.classList.add('like');
  imgLike.src = `images/like.svg`;
  arrAction[index].prepend(imgLike);
}

function unlike(index) {
  let arrBtn = [...document.querySelectorAll('.edit')];
  let arrAction = [...document.querySelectorAll('.actions')];
  console.log('unlike');
  songs[index].isLiked = false;
  arrBtn[index].textContent = 'like';
  arrBtn[index].classList.remove('img');
  arrAction[index].removeChild(arrAction[index].firstChild);
}